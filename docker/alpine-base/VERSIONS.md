# alpine-base

This file contains all software versions, that correspond to a version of this image itself. Read more about the [alpine-base image here][alpinebase].

## Latest

Same as v1.0.1

Usage: `hackinglab/alpine-base` or `hackinglab/alpine-base:latest`.

## v1.0.1

- [alpinelinux](Alpine Linux): v3.6
- [s6-overlay][s6-overlay]: v1.18.1.5
- [go-dnsmasq][godnsmasq]: v1.0.7


