# alpine-nginx

This file contains all software versions, that correspond to a version of this image itself. Read more about the [alpine-nginx image here][alpinenginx].

## Latest

Same as v1.0.1.

Usage: `hackinglab/alpine-nginx` or `hackinglab/alpine-nginx:latest`.

## v1.0.1

- [hackinglab/alpine-base: v1.0.1][hackinglab]
- [nginx][nginx]: v1.8.1

