# Mozilla User Preferences

/* Do not edit this file.
 *
 * If you make changes to this file while the application is running,
 * the changes will be overwritten when the application exits.
 *
 * To make a manual change to preferences, you can visit the URL about:config
 */

user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1490028605);
user_pref("app.update.lastUpdateTime.background-update-timer", 1457105658);
user_pref("app.update.lastUpdateTime.blocklist-background-update-timer", 1490028725);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1490091117);
user_pref("app.update.lastUpdateTime.datareporting-healthreport-lastDailyCollection", 1448615423);
user_pref("app.update.lastUpdateTime.experiments-update-timer", 1499111135);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1499021040);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 0);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1490090997);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.capacity", 358400);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.cache.disk.smart_size.use_old_max", false);
user_pref("browser.cache.frecency_experiment", 3);
user_pref("browser.customizemode.tip0.shown", true);
user_pref("browser.download.importedFromSqlite", true);
user_pref("browser.download.lastDir", "/home/hacker/Downloads");
user_pref("browser.download.panel.shown", true);
user_pref("browser.download.useDownloadDir", false);
user_pref("browser.migration.version", 43);
user_pref("browser.newtabpage.enhanced", true);
user_pref("browser.newtabpage.introShown", true);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.places.smartBookmarksVersion", 8);
user_pref("browser.preferences.advanced.selectedTabIndex", 4);
user_pref("browser.reader.detectedFirstArticle", true);
user_pref("browser.rights.3.shown", true);
user_pref("browser.safebrowsing.provider.google.lastupdatetime", "1499021020810");
user_pref("browser.safebrowsing.provider.google.nextupdatetime", "1499022737810");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1499021026001");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1499024626001");
user_pref("browser.search.countryCode", "CH");
user_pref("browser.search.region", "CH");
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20170612122310");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1499111106");
user_pref("browser.slowStartup.averageTime", 0);
user_pref("browser.slowStartup.samples", 0);
user_pref("browser.startup.homepage", "http://www.hacking-lab.com/");
user_pref("browser.startup.homepage_override.buildID", "20170612122310");
user_pref("browser.startup.homepage_override.mstone", "54.0");
user_pref("browser.tabs.remote.autostart.2", true);
user_pref("browser.toolbarbuttons.introduced.pocket-button", true);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"PanelUI-contents\":[\"edit-controls\",\"zoom-controls\",\"new-window-button\",\"privatebrowsing-button\",\"save-page-button\",\"print-button\",\"history-panelmenu\",\"fullscreen-button\",\"find-button\",\"preferences-button\",\"add-ons-button\",\"developer-button\"],\"addon-bar\":[\"addonbar-closebutton\",\"status-bar\"],\"PersonalToolbar\":[\"personal-bookmarks\"],\"nav-bar\":[\"urlbar-container\",\"search-container\",\"downloads-button\",\"home-button\",\"cookiemgr-button\",\"coomanPlus_button\",\"ecb-button\",\"mproxy-element-button\",\"firebug-badged-button\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"toolbar-menubar\":[\"menubar-items\"]},\"seen\":[\"loop-button\",\"pocket-button\",\"developer-button\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"TabsToolbar\",\"toolbar-menubar\",\"PanelUI-contents\",\"addon-bar\"],\"currentVersion\":6,\"newElementCount\":0}");
user_pref("browser.urlbar.daysBeforeHidingSuggestionsPrompt", 0);
user_pref("browser.urlbar.lastSuggestionsPromptDate", 20170321);
user_pref("datareporting.healthreport.lastDataSubmissionRequestedTime", "1448613207367");
user_pref("datareporting.healthreport.nextDataSubmissionTime", "1448613207366");
user_pref("datareporting.healthreport.service.firstRun", true);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1448613198287");
user_pref("datareporting.policy.firstRunTime", "1448613137285");
user_pref("datareporting.sessions.current.activeTicks", 15);
user_pref("datareporting.sessions.current.clean", true);
user_pref("datareporting.sessions.current.firstPaint", 29926);
user_pref("datareporting.sessions.current.main", 24151);
user_pref("datareporting.sessions.current.sessionRestored", 29650);
user_pref("datareporting.sessions.current.startTime", "1499111076698");
user_pref("datareporting.sessions.current.totalTime", 103);
user_pref("datareporting.sessions.currentIndex", 55);
user_pref("datareporting.sessions.previous.49", "{\"s\":1490028564605,\"a\":30,\"t\":231,\"c\":true,\"m\":6811,\"fp\":7983,\"sr\":11620}");
user_pref("datareporting.sessions.previous.50", "{\"s\":1490031420463,\"a\":3,\"t\":17,\"c\":true,\"m\":2580,\"fp\":5866,\"sr\":6068}");
user_pref("datareporting.sessions.previous.51", "{\"s\":1490090958211,\"a\":21,\"t\":245,\"c\":true,\"m\":7570,\"fp\":10907,\"sr\":11144}");
user_pref("datareporting.sessions.previous.52", "{\"s\":1490110237513,\"a\":3,\"t\":34,\"c\":true,\"m\":9200,\"fp\":11888,\"sr\":11756}");
user_pref("datareporting.sessions.previous.53", "{\"s\":1496387024130,\"a\":3,\"t\":21,\"c\":true,\"m\":1161,\"fp\":2146,\"sr\":4664}");
user_pref("datareporting.sessions.previous.54", "{\"s\":1499021004227,\"a\":9,\"t\":50,\"c\":true,\"m\":2340,\"fp\":4152,\"sr\":7277}");
user_pref("datareporting.sessions.prunedIndex", 48);
user_pref("distribution.canonical.bookmarksProcessed", true);
user_pref("dom.apps.lastUpdate.buildID", "20160922113459");
user_pref("dom.apps.lastUpdate.mstone", "49.0.1");
user_pref("dom.apps.reset-permissions", true);
user_pref("dom.mozApps.used", true);
user_pref("e10s.rollout.cohort", "disqualified-test");
user_pref("e10s.rollout.cohortSample", "0.925547");
user_pref("experiments.activeExperiment", false);
user_pref("extensions.blocklist.pingCountTotal", 3);
user_pref("extensions.blocklist.pingCountVersion", -1);
user_pref("extensions.bootstrappedAddons", "{\"firebug@software.joehewitt.com\":{\"version\":\"2.0.19\",\"type\":\"extension\",\"descriptor\":\"/home/hacker/.mozilla/firefox/6x9a4zlk.Browser 2/extensions/firebug@software.joehewitt.com.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"e10srollout@mozilla.org\":{\"version\":\"1.50\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/e10srollout@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"firefox@getpocket.com\":{\"version\":\"1.0.5\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/firefox@getpocket.com.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"webcompat@mozilla.org\":{\"version\":\"1.1\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/webcompat@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"aushelper@mozilla.org\":{\"version\":\"2.0\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/aushelper@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"screenshots@mozilla.org\":{\"version\":\"6.6.0\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/screenshots@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false}}");
user_pref("extensions.cookiemgr.firstRunComplete", true);
user_pref("extensions.cookiesmanagerplus.autoupdate", true);
user_pref("extensions.cookiesmanagerplus.version", "1.13.3");
user_pref("extensions.databaseSchema", 19);
user_pref("extensions.e10s.rollout.blocklist", "");
user_pref("extensions.e10s.rollout.hasAddon", false);
user_pref("extensions.e10s.rollout.policy", "50allmpc");
user_pref("extensions.e10sBlockedByAddons", true);
user_pref("extensions.e10sMultiBlockedByAddons", true);
user_pref("extensions.enabledAddons", "proxyselector%40mozilla.org:1.31.1-signed,cookiemgr%40jayapal.com:5.12,%7Bbb6bc1bb-f824-4702-90cd-35e2fb24f25d%7D:1.13.3,%7B4cc4a13b-94a6-7568-370d-5f9de54a9c7f%7D:2.7.1-signed.1-signed,%7B8f8fe09b-0bd3-4470-bc1b-8cad42b8203a%7D:0.17.1-signed.1-signed,%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:54.0");
user_pref("extensions.firebug.DBG_BREAKNOTIFICATION", false);
user_pref("extensions.firebug.DBG_BREAKONERROR", false);
user_pref("extensions.firebug.DBG_BREAKONNEXT", false);
user_pref("extensions.firebug.DBG_BREAKPOINTMODULE", false);
user_pref("extensions.firebug.DBG_BREAKPOINTPANEL", false);
user_pref("extensions.firebug.DBG_BREAKPOINTS", false);
user_pref("extensions.firebug.DBG_BREAKPOINTSTORE", false);
user_pref("extensions.firebug.DBG_BREAKPOINTTOOL", false);
user_pref("extensions.firebug.DBG_DEBUGGER", false);
user_pref("extensions.firebug.DBG_DEBUGGERCLIENT", false);
user_pref("extensions.firebug.DBG_DEBUGGERHALTER", false);
user_pref("extensions.firebug.DBG_DEBUGGER_COMMANDS", false);
user_pref("extensions.firebug.DBG_DOMBASETREE", false);
user_pref("extensions.firebug.DBG_DOMPANEL", false);
user_pref("extensions.firebug.DBG_FUNCTIONMONITOR", false);
user_pref("extensions.firebug.DBG_HTMLMODULE", false);
user_pref("extensions.firebug.DBG_NETCACHEREADER", false);
user_pref("extensions.firebug.DBG_PANELSELECTOR", false);
user_pref("extensions.firebug.DBG_QUICKINFOBOX", false);
user_pref("extensions.firebug.DBG_RETURNVALUEMODIFIER", false);
user_pref("extensions.firebug.DBG_SCOPECLIENT", false);
user_pref("extensions.firebug.DBG_SCRIPTPANEL", false);
user_pref("extensions.firebug.DBG_SCRIPTPANELLINEUPDATER", false);
user_pref("extensions.firebug.DBG_SCRIPTPANELWARNING", false);
user_pref("extensions.firebug.DBG_SOURCEEDITOR", false);
user_pref("extensions.firebug.DBG_SOURCEFILE", false);
user_pref("extensions.firebug.DBG_SOURCETOOL", false);
user_pref("extensions.firebug.DBG_SPY", false);
user_pref("extensions.firebug.DBG_STATUSPATH", false);
user_pref("extensions.firebug.DBG_TABCLIENT", false);
user_pref("extensions.firebug.DBG_TABCONTEXT", false);
user_pref("extensions.firebug.DBG_TABLEREP", false);
user_pref("extensions.firebug.DBG_TOGGLESIDEPANELS", false);
user_pref("extensions.firebug.DBG_WATCHPROVIDER", false);
user_pref("extensions.firebug.currentVersion", "2.0.19");
user_pref("extensions.firebug.toolbarCustomizationDone2", true);
user_pref("extensions.getAddons.cache.lastUpdate", 1499021009);
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.hotfix.lastVersion", "20170302.01");
user_pref("extensions.lastAppVersion", "54.0");
user_pref("extensions.lastPlatformVersion", "54.0");
user_pref("extensions.livehttpheaders.exclude", false);
user_pref("extensions.livehttpheaders.excludeRegexp", ".gif$|.jpg$|.ico$|.css$|.js$");
user_pref("extensions.livehttpheaders.filter", false);
user_pref("extensions.livehttpheaders.filterRegexp", "/$|.html$");
user_pref("extensions.livehttpheaders.mode", 1);
user_pref("extensions.livehttpheaders.style", 0);
user_pref("extensions.livehttpheaders.tab", false);
user_pref("extensions.pendingOperations", false);
user_pref("extensions.proxyselector.first.run", false);
user_pref("extensions.proxyselector.proxy.current", "None");
user_pref("extensions.proxyselector.proxy.rdf.lastupdate", -194949487);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.ui.dictionary.hidden", true);
user_pref("extensions.ui.experiment.hidden", true);
user_pref("extensions.ui.lastCategory", "addons://search/cookie");
user_pref("extensions.ui.locale.hidden", true);
user_pref("extensions.xpiState", "{\"app-profile\":{\"{8f8fe09b-0bd3-4470-bc1b-8cad42b8203a}\":{\"d\":\"/home/hacker/.mozilla/firefox/6x9a4zlk.Browser 2/extensions/{8f8fe09b-0bd3-4470-bc1b-8cad42b8203a}\",\"e\":true,\"v\":\"0.17.1-signed.1-signed\",\"st\":1499069618000,\"mt\":1499062222000},\"cookiemgr@jayapal.com\":{\"d\":\"/home/hacker/.mozilla/firefox/6x9a4zlk.Browser 2/extensions/cookiemgr@jayapal.com\",\"e\":true,\"v\":\"5.12\",\"st\":1499069618000,\"mt\":1499062222000},\"{bb6bc1bb-f824-4702-90cd-35e2fb24f25d}\":{\"d\":\"/home/hacker/.mozilla/firefox/6x9a4zlk.Browser 2/extensions/{bb6bc1bb-f824-4702-90cd-35e2fb24f25d}\",\"e\":true,\"v\":\"1.13.3\",\"st\":1499069618000,\"mt\":1499062222000},\"proxyselector@mozilla.org\":{\"d\":\"/home/hacker/.mozilla/firefox/6x9a4zlk.Browser 2/extensions/proxyselector@mozilla.org.xpi\",\"e\":true,\"v\":\"1.31.1-signed\",\"st\":1499062222000},\"firebug@software.joehewitt.com\":{\"d\":\"/home/hacker/.mozilla/firefox/6x9a4zlk.Browser 2/extensions/firebug@software.joehewitt.com.xpi\",\"e\":true,\"v\":\"2.0.19\",\"st\":1499062222000},\"{4cc4a13b-94a6-7568-370d-5f9de54a9c7f}\":{\"d\":\"/home/hacker/.mozilla/firefox/6x9a4zlk.Browser 2/extensions/{4cc4a13b-94a6-7568-370d-5f9de54a9c7f}\",\"e\":true,\"v\":\"2.7.1-signed.1-signed\",\"st\":1499069618000,\"mt\":1499062222000}},\"app-system-defaults\":{\"screenshots@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/screenshots@mozilla.org.xpi\",\"e\":true,\"v\":\"6.6.0\",\"st\":1497273884000},\"aushelper@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/aushelper@mozilla.org.xpi\",\"e\":true,\"v\":\"2.0\",\"st\":1497273883000},\"e10srollout@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/e10srollout@mozilla.org.xpi\",\"e\":true,\"v\":\"1.50\",\"st\":1497273883000},\"webcompat@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/webcompat@mozilla.org.xpi\",\"e\":true,\"v\":\"1.1\",\"st\":1497273884000},\"firefox@getpocket.com\":{\"d\":\"/usr/lib/firefox/browser/features/firefox@getpocket.com.xpi\",\"e\":true,\"v\":\"1.0.5\",\"st\":1497273884000}},\"app-global\":{\"{972ce4c6-7e08-4474-a285-3208198ce6fd}\":{\"d\":\"/usr/lib/firefox/browser/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}.xpi\",\"e\":true,\"v\":\"54.0\",\"st\":1497273883000}}}");
user_pref("extensions.{4cc4a13b-94a6-7568-370d-5f9de54a9c7f}.firstRun24", false);
user_pref("gecko.buildID", "20160823121617");
user_pref("gecko.mstone", "48.0.2");
user_pref("idle.lastDailyNotification", 1490091087);
user_pref("media.gmp-gmpopenh264.abi", "x86_64-gcc3");
user_pref("media.gmp-gmpopenh264.lastUpdate", 1474798947);
user_pref("media.gmp-gmpopenh264.version", "1.6");
user_pref("media.gmp-manager.buildID", "20170612122310");
user_pref("media.gmp-manager.lastCheck", 1499111167);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("media.webrtc.debug.aec_log_dir", "/tmp");
user_pref("media.webrtc.debug.log_file", "/tmp/WebRTC.log");
user_pref("network.cookie.prefsMigrated", true);
user_pref("network.predictor.cleaned-up", true);
user_pref("network.proxy.type", 0);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);
user_pref("places.database.lastMaintenance", 1490091087);
user_pref("places.history.expiration.transient_current_max_pages", 122334);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("plugin.importedState", true);
user_pref("privacy.sanitize.migrateClearSavedPwdsOnExit", true);
user_pref("privacy.sanitize.migrateFx3Prefs", true);
user_pref("security.disable_button.openCertManager", false);
user_pref("services.blocklist.addons.checked", 1490023704);
user_pref("services.blocklist.clock_skew_seconds", 5021);
user_pref("services.blocklist.last_etag", "\"1490023533943\"");
user_pref("services.blocklist.last_update_seconds", 1490023704);
user_pref("services.blocklist.onecrl.checked", 1490023704);
user_pref("services.blocklist.plugins.checked", 1490023704);
user_pref("signon.importedFromSqlite", true);
user_pref("storage.vacuum.last.index", 0);
user_pref("storage.vacuum.last.places.sqlite", 1490091087);
user_pref("toolkit.startup.last_success", 1499111100);
user_pref("toolkit.telemetry.cachedClientID", "1874cf4a-559e-4eb7-b000-088eb70a19ee");
user_pref("toolkit.telemetry.previousBuildID", "20170612122310");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("ui.key.menuAccessKeyFocuses", false);
